#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

__version__ = "0.1"
__author__  = "Marotte"
__licence__ = "GPL"
__status__  = "Experimental"

import sys

try:

    import pickle

except ImportError as e:

    print(str(e), file=sys.stderr)
    print('Cannot find the module(s) listed above. Exiting.', file=sys.stderr)
    sys.exit(99)
    
class VF:

    def __init__(self):
        
        self.sentences = []
        self.sentence_dict = {}
    
    def log(self,message,f=sys.stderr):
        
        print(str(message),file=f)

    def getWords(self, word):
        
        pass

    def OKWord(self, word):

        try:
            if not word[0].isalpha():
                del word[0]
                if len(word) < 2: return ''.join(word)
                else: return self.OKWord(word)
            else: return ''.join(word)
        except IndexError: return ''.join(word)

    def isEndingWord(self, word):    
        
        try:
            if word[-1:][0] in ['.','!','?','…',':','-']: return True
            else: return False
        except IndexError: return False
        
    def readFile(self, f):
        
        sentence = []
        word = []
        while True:
            c = f.read(1)
            if not c:
                break
            if c not in [' ','\n']:
                word.append(c)
            else:
                ok = self.OKWord(word)
                if self.isEndingWord(word):
                    if ok not in ['','.']:
                        sentence.append(ok)
                        self.sentences.append(' '.join(sentence).strip())
                    sentence = []
                    word = []
                else:
                    if ok not in ['','.']:
                        sentence.append(ok)
                    word = []

    def makeSentenceDict(self):
        
        self.log('Making sentence dictionnary…')
        for s in self.sentences:
            s = s.replace('\n','')
            try:
                seen = self.sentence_dict[s]
                self.sentence_dict[s] = seen + 1
            except KeyError:
                self.sentence_dict[s] = 1

if __name__ == '__main__':
    
    sys.exit(1)
